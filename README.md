# FHIR-pseudonym Adapter
A FHIR RESTful extension that consumes FHIR Bundle resource and generates a corresponding pseudonym.
It also performs two-factor validation of the FHIR resources inthe Bundle and also the required parameters needed for the pseudonymization process.

## Try it out at:
https://app.swaggerhub.com/apis-docs/jonathanokereke/fhir-pseudonym_adapter_api_documentation/4.0

## Docker Implementation
Prerequisite: Java 11, Maven 3.6, Docker 18, FHIR-R4 (RESTful-client)
- Create network domain for all containers

```docker
docker network create fhir-pseudonym-network
```

- Start Mainzelliste database

```docker
docker run -d \
--name db \
--network fhir-pseudonym-network \
--restart always \
-e POSTGRES_DB=mainzelliste \
-e POSTGRES_USER=mainzelliste \
-e POSTGRES_PASSWORD=medickoeln2020 \
postgres:9.5-alpine
```

- Instantiate Mainzelliste server

```docker
docker run -d \
--name mainzelliste \
--network fhir-pseudonym-network \
--restart always \
-p 7304:8080 \
-e ML_REVERSEPROXY_FQDN=mainzelliste \
-e ML_REVERSEPROXY_SSL=false \
-e ML_DB_PASS=medickoeln2020 \
-e POSTGRES_DB=mainzelliste \
-e POSTGRES_USER=mainzelliste \
-e POSTGRES_PASSWORD=medickoeln2020 \
-e ML_API_KEY=39bd9bee-6e5b-4f7c-87ae-52c272ef7fac \
-e ML_ALLOWEDREMOTEADDRESSES=0.0.0.0/0 \
medicalinformatics/mainzelliste:1.8-latest
```

- Start database instance for FHIR-pseudonym adapter

```docker
docker run -d \
--name fhir-pseudonym-adapter-db \
--network fhir-pseudonym-network \
--restart always \
-e POSTGRES_USER=postgres \
-e POSTGRES_PASSWORD=medic2020 \
-e POSTGRES_DB=fhir-pseudonym-adapter_db \
postgres:latest
```

- Fire up the FHIR-pseudonym service
```docker
docker run -d \
--name fhir-pseudonym-adapter \
--network fhir-pseudonym-network \
--restart always \
-p 8083:8083 \
jonathanokereke/fhir-pseudonym-adapter:latest
```

By default, the FHIR-pseudonym service API runs at port `` 8083 ``

- Now, you can start generating pseudonyms for corresponding FHIR (Patient) resource in a Bundle at: 
```
            http://localhost:8083/api/v4/fhir-pseudonym-adapter/bundle-resource/bundle
```

## Required FHIR-payload
- **Parameters**: name-family, name-given, birthDate, address-postalCode
```
            http://localhost:8083/api/v4/fhir-pseudonym-adapter/metadata/required
```
```json
{
    "resourceType": "Patient",
    "active": true,
    "name": [
        {
            "family": "*",
            "given": [
                "*"
            ]
        }
    ],
    "birthDate": "*",
    "address": [
        {
            "postalCode": "*****"
        }
    ]
}
```
- Capability Statement (Extended)
```
            http://localhost:8083/api/v4/fhir-pseudonym-adapter/metadata/fhir
```

API Documentation also available at:

https://app.swaggerhub.com/apis-docs/jonathanokereke/fhir-pseudonym_adapter_api_documentation/2.0#/
